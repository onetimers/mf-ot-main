import { AppRoutingModule } from './app-routing.module';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CodeLoginComponent } from './code-login/code-login.component';
import { ReactiveFormsModule } from '@angular/forms';

import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { AmplifyAuthenticatorModule } from '@aws-amplify/ui-angular';
import { Amplify } from 'aws-amplify';

Amplify.configure({
  region: 'us-east-1',
  userPoolId: 'us-east-1_2wX7OGUtl',
  userPoolWebClientId: '6m4vt7cbuk6hh89ggl2fb23ig0',
  mandatorySignIn: false,
  cookieStorage: {
    domain: 'localhost',
    secure: false,
    path: '/',
    expires: 365,
  },
});

@NgModule({
  declarations: [
    AppComponent,
    CodeLoginComponent,
  ],
  imports: [
    BrowserModule,
    AmplifyAuthenticatorModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
