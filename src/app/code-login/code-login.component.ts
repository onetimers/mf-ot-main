import { Component, OnInit } from '@angular/core';
import { Auth } from 'aws-amplify';

@Component({
  selector: 'code-login',
  templateUrl: './code-login.component.html',
  styleUrls: ['./code-login.component.scss']
})
export class CodeLoginComponent implements OnInit {

  constructor(
  ) { }

  //CHECK IF IT MAKES SENSE TO DO THE ONETIMERS APP, WE WOULD HAVE TO DEVELOP TWO CLIENTS (CELLPHONE SEEKERS / DESKTOP POSTERS)
  //WHAT IF WE MAKE A KIND OF NATIVE APP AND TRANSPILE IT TO WEB / MOBILE?
  ngOnInit(): void {
    //this.signUp('mtsperazzo@gmail.com', 'Landrover7!', '+5491169401880', 'Matias', 'Perazzo', '05/03/1995', 'Calle Siempre viva')
    //this.confirmSignUp('maperazzo@itba.edu.ar', '024492')
    //LOGIN WITH maperazzo@itba.edu.ar and check wtf are the credentials stored
  }

  //SIGN IN SHOULD REQUIRE CODE CONFIRMATION
  async signUp(username: string, password: string, phone_number: string, given_name: string, family_name: string, birthdate: string, address: string) {
    try {
      const { user } = await Auth.signUp({
        username,
        password,
        attributes: {
          phone_number,
          given_name,
          family_name,
          birthdate,
          address
        },
        autoSignIn: { // optional - enables auto sign in after user is confirmed
          enabled: true,
        }
      });
      console.log(user);
    } catch (error) {
      console.log('error signing up:', error);
    }
  }

  //Re-send sign up confirmation code
  async resendConfirmationCode(username: string) {
    try {
      await Auth.resendSignUp(username);
      console.log('code resent successfully');
    } catch (err) {
      console.log('error resending code: ', err);
    }
  }

  //Code is valid for 24 hours (edge case should be handled)
  async confirmSignUp(username: string, code: string) {
    try {
      await Auth.confirmSignUp(username, code);
    } catch (error) {
      console.log('error confirming sign up', error);
    }
  }
}
